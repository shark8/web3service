from fastapi import FastAPI, Query
from fastapi.encoders import jsonable_encoder
from typing import Optional
from web3eth import web3worker

app = FastAPI()


@app.get("/")
async def root():
    """
    Simple getter
    """
    return {"message": "Shark Web3 microservice"}

@app.post("/nft/{ipfs_hash}")
async def mint_nft(
    ipfs_hash: str = Query(..., max_length=46, min_length=46), 
):
    """
    Mint NFT with ipfs hash in tokenURI
    `ipfs_hash` 46-byte string like QmafZq1ZYLGvQJKAVt8PM3Y78to5wQrsazyasqdBCB9XU9 
    """
    r = web3worker.handle_mint_nft(ipfs_hash)
    #print(r)
    item = {
        'ipfs_hash': ipfs_hash, 
        'state': 'queued_up', 
        'tx_hash': r,
        'link': '{}{}'.format(web3worker.config.TX_LINK_PREFIX, r)
        
    }
    return item


@app.get("/tx/{tx_hash}")
async def get_tx_status(tx_hash: str):
    """
    Returns transaction state from blockchain
    """
    (r, log) = web3worker.get_tx_receipt(tx_hash)
    return {
        "tx_hash": tx_hash, 
        "tx_status": r.status,
        'tokenId': log[0]['args']['tokenId']
    }

@app.get("/tokenURI/{token_id}")
async def get_token_uri(token_id: str):
    r = web3worker.get_token_uri(token_id)
    return {"token_id": token_id, "tokenURI": r}    

