# -*- coding: utf-8 -*-
import os
import logging
import json


abi_file=open('./web3eth/erc721mint.abi')
abi_str = abi_file.read()
APP_LOGLEVEL=os.environ.get('APP_LOGLEVEL',logging.DEBUG)# logging.DEBUG-10 .....logging.CRITICAL-50
WEB3_PROVIDER = os.environ.get('WEB3_PROVIDER','http://127.0.0.1:8545')
ADDRESS_721 = os.environ.get('ADDRESS_EXOTOKEN','0x77E06E7175F989140AC42E95E9F986E5C990cb7d')
ADDRESS_OPERATOR = os.environ.get('ADDRESS_OPERATOR','0xE71978b1696a972b1a8f724A4eBDB906d9dA0885')
ADDRESS_OPERATOR_PRIVKEY = os.environ.get('ADDRESS_OPERATOR_PRIVKEY',0)
ERC721_ABI = json.loads(abi_str)
GAS_PRICE=os.environ.get('GAS_PRICE', 21)
TX_LINK_PREFIX = os.environ.get('TX_LINK_PREFIX', 'https://')

