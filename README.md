## Blockchain connector service
Microservice expose API for interaction with NFT smart contract. Implementation
based on FastAPI framework,  
[Documentation and test environment](https://utabot.iber.group:8888/docs)  

Just don't forget topup `operator's` account with native blockchain asset (BNB, ETH, ..)  
for success NFT minting.

### Local development
#### Recomended folder structure

```bash
cd fastapi
docker build -f ./DockerfileLocal -t fastapiweb3:local .
docker-compose -p fastapi -f docker-compose-local.yaml up -d

docker-compose -p fastapi -f docker-compose-web3local.yaml up

docker run -it -v$(pwd):/app fastapiweb3:local bash

#Logs

```

